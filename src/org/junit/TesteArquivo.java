package org.junit;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class TesteArquivo {

    final String ARQUIVO = "C:\\Users\\Luiz\\Desktop\\trabalho\\teste.txt";
    @Test
    public void teste(){
        
        Cliente cliente1 = new Cliente(1, "CLAUDNEY SANTANA", "96957286");
        Cliente cliente2 = new Cliente(2, "LUIZ EUGENIO", "387455641");
        
        gravaCliente(cliente1);
        gravaCliente(cliente2);
    }

    @Test
    public void teste2(){
        Cliente res = pesquisarId(3);
        System.out.println("aaaaa");
    }
    
    private void gravaCliente(Cliente cliente1) {
        StringBuilder sb = new StringBuilder();
        sb.append(completaEspacos(cliente1.id.toString(),5));
        sb.append(completaEspacos(cliente1.nome,100));
        sb.append(completaEspacos(cliente1.telefone,20));
        try {
            FileWriter f = new FileWriter(ARQUIVO,true);
            f.append(sb.toString()+"\r\n");
            f.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
 
    private Cliente criaObjeto(String linha){
        Cliente cliente = new Cliente(Integer.parseInt(linha.substring(0, 5).trim()), 
                linha.substring(5, 105),
                linha.substring(105, 125));
        return cliente;
    }

    @SuppressWarnings("resource")
    public Cliente pesquisarId(Integer Id){
        try {
            FileReader fileReader = new FileReader(ARQUIVO);
            BufferedReader lerArq = new BufferedReader(fileReader);
            String linha = lerArq.readLine();
            while (linha != null) {
                if(linha.substring(0,5).trim().equals(Id.toString())){
                    return criaObjeto(linha);
                }
                linha = lerArq.readLine();
            }
            lerArq.close();
            fileReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    private String completaEspacos(String valor, int quantidade){
        if(valor.length() < quantidade){
            int qtdFalta = Math.abs(valor.length() - quantidade);
            for (int i = 0; i < qtdFalta; i++) {
                valor += " ";
            }
        }
        return valor;
    }
    
    private class Cliente{
        
        Integer id;
        String nome;
        String telefone;
        
        public Cliente(Integer id, String nome, String telefone) {
            super();
            this.id = id;
            this.nome = nome;
            this.telefone = telefone;
        }
        
    }
}

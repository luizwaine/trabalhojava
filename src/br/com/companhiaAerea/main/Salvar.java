package br.com.companhiaAerea.main;

import java.io.IOException;

public interface Salvar {
	
	public boolean salvar()throws IOException;

}

package br.com.companhiaAerea.main;

import br.com.companhiaAerea.models.Pessoas;

public interface Pessoa {
	public boolean salvar(Pessoas pessoa);
	public boolean deletar();
	public void listarPessoas();
	
}

package br.com.companhiaAerea.main;

import br.com.companhiaAerea.models.Aviao;

public interface CriaAviao {
	
	public void listarAvioes();
	public boolean deletar();
	public boolean editarAviao();
	public Aviao pesquisarAviao();
	public boolean salvar();

}

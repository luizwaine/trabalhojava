package br.com.companhiaAerea.main;

import java.io.IOException;
import java.util.Scanner;

import br.com.companhiaAerea.models.Aviao;
import br.com.companhiaAerea.models.Cidade;
import br.com.companhiaAerea.models.Cliente;
import br.com.companhiaAerea.models.Login;
import br.com.companhiaAerea.models.Pessoas;
import br.com.companhiaAerea.models.Usuario;
import br.com.companhiaAerea.models.Voo;

public class Principal {
	
	public static final String ARQUIVO_AVIOES = "C:\\Users\\Luiz\\Desktop\\trabalho\\aviao.txt";
	public static final String ARQ_TEMP_AVIOES = "C:\\Users\\Luiz\\Desktop\\trabalho\\temporario_aviao.txt";
	public static final String ARQUIVO_CIDADES = "C:\\Users\\Luiz\\Desktop\\trabalho\\cidades.txt";
	public static final String ARQ_TEMP_CIDADES = "C:\\Users\\Luiz\\Desktop\\trabalho\\temporario_cidades.txt";
	public static final String ARQUIVO_VOOS = "C:\\Users\\Luiz\\Desktop\\trabalho\\voos.txt";
	public static final String ARQ_TEMP_VOOS = "C:\\Users\\Luiz\\Desktop\\trabalho\\temporario_voos.txt";
	public static final String ARQUIVO_PESSOAS = "C:\\Users\\Luiz\\Desktop\\trabalho\\pessoas.txt";
	public static final String ARQ_TEMP_PESSOAS = "C:\\Users\\Luiz\\Desktop\\trabalho\\temporario_pessoas.txt";
	public static final String ARQUIVO_CLIENTES = "C:\\Users\\Luiz\\Desktop\\trabalho\\clientes.txt";
	public static final String ARQ_TEMP_CLIENTES = "C:\\Users\\Luiz\\Desktop\\trabalho\\temporario_clientes.txt";
	public static final String ARQUIVO_USUARIOS = "C:\\Users\\Luiz\\Desktop\\trabalho\\usuarios.txt";
	public static final String ARQ_TEMP_USUARIOS = "C:\\Users\\Luiz\\Desktop\\trabalho\\temporario_usuarios.txt";

	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {
		Scanner read = new Scanner(System.in);
		Integer opcao;
		Login login = new Login();
		
		System.out.println("Digite 1 se voc� for cliente ou 2 para usu�rio!");
		opcao = read.nextInt();
		
		if(opcao == 2 && login.loginUsuario() == true){			
				menu();
		}
		if(opcao == 1 && login.loginCliente()){
			
		}
		else
			System.out.println("Sua senha est� incorreta, digite-a novamente:");
	}

	static void menu() throws IOException {
		Aviao aviao = new Aviao(null, null, null, 0);
		Cidade cidade = new Cidade(null, null, null, null);
		Voo voo = new Voo(null, null, null, null, null, null, null);		
		Cliente cliente = new Cliente(null, 0, null, null, null, null, null);
		Cliente cliente2parametros = new Cliente(0, 0);
		Usuario usuario = new Usuario(null, null, null, null, null, null, null, null);

		int opcao = 0;

		do {

			@SuppressWarnings("resource")
			Scanner read = new Scanner(System.in);
			
			@SuppressWarnings("unused")
			Integer menu;
			System.out.println("\n\n");
			System.out.println("------------------ESCOLHA UMA DAS OP��ES ABAIXO--------------------\n");
			System.out.println("\n------------------OP��ES CADASTRO--------------------\n");
			System.out.println(" | 1|- Cadastrar avi�es!");
			System.out.println(" | 2|- Cadastrar cidades!");			
			System.out.println(" | 4|- Cadastrar usu�rios!");
			System.out.println(" | 5|- Cadastrar clientes!");
			System.out.println(" | 6|- Cadastrar v�os!");
			System.out.println(" | 7|- Emitir passagens!");
			System.out.println("\n------------------OP��ES AVI�ES--------------------\n");
			System.out.println(" | 8|- Pesquisar avi�es!");
			System.out.println(" | 9|- Listar todos avi�es!");
			System.out.println(" |10|- Deletar um avi�o!");
			System.out.println(" |11|- Editar avi�o!");
			System.out.println("\n------------------OP��ES CIDADES--------------------\n");
			System.out.println(" |12|- Pesquisar cidades!");
			System.out.println(" |13|- Listar todas cidades!");
			System.out.println(" |14|- Deletar uma cidade!");
			System.out.println(" |15|- Editar Cidade!");
			System.out.println("\n------------------OP��ES V�OS--------------------\n");
			System.out.println(" |16|- Pesquisar v�o!");
			System.out.println(" |17|- Listar todos v�os!");
			System.out.println(" |18|- Deletar v�o!");
			System.out.println(" |19|- Editar v�o!");		
			System.out.println("\n------------------OP��ES CLIENTE--------------------\n");
			System.out.println(" |20|- Pesquisar clientes!");
			System.out.println(" |21|- Listar clientes!");
			System.out.println(" |22|- Excluir Clientes!");
			System.out.println(" |23|- Editar informa��es do cliente!");
			System.out.println("\n------------------OP��ES USU�RIOS--------------------\n");
			System.out.println(" |24|- Pesquisar usu�rios!");
			System.out.println(" |25|- Listar usu�rios!");
			System.out.println(" |26|- Excluir usu�rios!");
			System.out.println(" |27|- Editar informa��es do usu�rio!");
			
			
			System.out.println(" |00|- SAIR");

			int choice = read.nextInt();
//			menu = read.nextInt();
//			ArrayList<Object> array = new ArrayList<Object>();

			switch (choice) {

			case 1:
				aviao.salvar();
				break;				
			case 2:
				cidade.salvar();
				break;	
			case 4:
				usuario.salvarUsuarios();
				break;
			case 5:
				cliente.salvarClientes();
				break;
			case 6:
				voo.salvarVoo();
				break;	
			case 8:				
				aviao.pesquisarAviao(); 
				break;				
			case 9:
				aviao.listarAvioes();
				break;				
			case 10:
				aviao.deletar();
				break;				
			case 11:
				aviao.editarAviao();
				break;				
			case 12:
				cidade.pesquisarCidade();
				break;				
			case 13:
				cidade.listarCidades();
				break;				
			case 14:
				cidade.deletar();
				break;				
			case 15:
				cidade.editarCidade();
				break;
			case 16:
				voo.pesquisarVoo();
				break;				
			case 17:
				voo.listarVoos();
				break;				
			case 18:
				voo.deletar();
				break;	
			case 19:
				voo.editarVoo();
				break;			
			case 20:
				cliente.pesquisarCliente();
				break;				
			case 21:
				cliente.listarClientes();
				break;				
			case 22:
				cliente.deletarClientes();
				break;	
			case 23:
				cliente.editarCliente();
				break;
			case 24:
				usuario.pesquisarUsuario();
				break;				
			case 25:
				usuario.listarUsuarios();
				break;				
			case 26:
				usuario.deletarUsuarios();
				break;	
			case 27:
				usuario.editarUsuario();
				break;	
			case 0:			
				System.exit(0);
				break;
			}

		} while (opcao <= 28);
	}
}

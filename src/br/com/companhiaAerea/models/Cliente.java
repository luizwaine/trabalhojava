package br.com.companhiaAerea.models;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import br.com.companhiaAerea.main.Pessoa;
import br.com.companhiaAerea.main.Principal;

public class Cliente extends Pessoas implements Pessoa {
	
	protected Integer senha;
	
	final File ARQUIVO_USUARIO = new File(Principal.ARQUIVO_CLIENTES);
    final File TEMPORARIO_CLIENTES = new File(Principal.ARQ_TEMP_CLIENTES);

	public Integer getSenha() {
		return senha;
	}



	public void setSenha(Integer senha) {
		this.senha = senha;
	}


// SOBRECARGA DE CONSTRUTOR
	public Cliente(String nome, Integer cpf, String rg, String telefone, String email, String endereco, Integer senha) {
		super(nome, cpf, rg, telefone, email, endereco);
		this.senha = senha;
		
	}
//SOBRECARGA DE CONSTRUTOR	
	public Cliente(Integer cpf,Integer senha) {
		super(nome, cpf, rg, telefone, email, endereco);
		this.senha = senha;
		
	}
	
	protected void  gravaClinte(Cliente cliente){
		StringBuilder sb = new StringBuilder();
		sb.append(completaEspacos(Pessoas.nome, 20));
		sb.append(completaEspacos(Pessoas.cpf.toString(), 20));
		sb.append(completaEspacos(Pessoas.rg, 20));
		sb.append(completaEspacos(Pessoas.telefone, 20));
		sb.append(completaEspacos(Pessoas.email, 20));
		sb.append(completaEspacos(Pessoas.endereco, 20));
		sb.append(completaEspacos(cliente.senha.toString(), 20));
		
		try {
			FileWriter f = new FileWriter(ARQUIVO_USUARIO, true);
			f.append(sb.toString() + "\r\n");
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
//SOBRECARGA DE CONSTRUTOR	
	protected void  gravaClinte(Integer cpf, Integer senha, Cliente cliente){
		StringBuilder sb = new StringBuilder();		
		sb.append(completaEspacos(Pessoas.cpf.toString(), 20));		
		sb.append(completaEspacos(cliente.senha.toString(), 20));
		
		try {
			FileWriter f = new FileWriter(ARQUIVO_USUARIO, true);
			f.append(sb.toString() + "\r\n");
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected void  gravaArquivoTemporarioClinte(Cliente cliente){
		StringBuilder sb = new StringBuilder();
		sb.append(completaEspacos(Pessoas.nome, 20));
		sb.append(completaEspacos(Pessoas.cpf.toString(), 20));
		sb.append(completaEspacos(Pessoas.rg, 20));
		sb.append(completaEspacos(Pessoas.telefone, 20));
		sb.append(completaEspacos(Pessoas.email, 20));
		sb.append(completaEspacos(Pessoas.endereco, 20));
		sb.append(completaEspacos(cliente.senha.toString(), 20));
		
		try {
			FileWriter f = new FileWriter(TEMPORARIO_CLIENTES, true);
			f.append(sb.toString() + "\r\n");
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected String completaEspacos(String valor, int quantidade) {
		if (valor.length() < quantidade) {
			int qtdFalta = Math.abs(valor.length() - quantidade);
			for (int i = 0; i < qtdFalta; i++) {
				valor += " ";
			}
		}
		return valor;
	}

	private Cliente criaObjeto(String linha) {
		Cliente cliente = new Cliente(linha.substring(0, 20), 
				Integer.parseInt(linha.substring(20, 40).trim()), 
				linha.substring(40, 60),
				linha.substring(60, 80),
				linha.substring(80, 100),
				linha.substring(100, 120),
				Integer.parseInt(linha.substring(120, 140).trim()));
		return cliente;
	}
	
//-------------------------------SOBRECARGA DE M�TODOS--------------------------	
	@SuppressWarnings("unused")
	private Cliente criaObjeto(String linha, Cliente cliente) {
		 cliente = new Cliente(
				Integer.parseInt(linha.substring(0, 20).trim()), 				
				Integer.parseInt(linha.substring(20, 40).trim()));
		return cliente;
	}

	@SuppressWarnings("resource")
	public boolean salvarClientes() {
		String linha = "";
		if (!ARQUIVO_USUARIO.exists())
			try {
				ARQUIVO_USUARIO.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		Cliente cliente = new Cliente(nome, cpf, rg, telefone, email, endereco, senha);

		Scanner read = new Scanner(System.in);

		System.out.println("Informe os dados da abaixo: ");

		System.out.println("Nome do cliente: ");
		String nome = read.next();
		cliente.setNome(nome);

		System.out.println("CPF: ");
		Integer cpf = read.nextInt();
		cliente.setCpf(cpf);

		System.out.println("RG: ");
		String rg = read.next();
		cliente.setRg(rg);		

		System.out.println("Telefone: ");
		String telefone = read.next();
		cliente.setTelefone(telefone);

		System.out.println("Email: ");
		String email = read.next();
		cliente.setEmail(email);
		
		System.out.println("Endere�o: ");
		String endereco = read.next();
		cliente.setEndereco(endereco);
		
		System.out.println("Senha: ");
		Integer senha = read.nextInt();
		cliente.setSenha(senha);
		
		try {

			FileReader fileReader = new FileReader(ARQUIVO_USUARIO);
			BufferedReader lerArq = new BufferedReader(fileReader);
			linha = lerArq.readLine();
			while (linha != null) {
				if (linha.substring(20, 40).trim().equals(cpf.toString())) {
					System.out.println("Este cliente j� esta cadastrado!");
					return false;
				} else {
					// System.out.println(aviao + "\n\n");
					linha = lerArq.readLine();
				}

			}

			gravaClinte(cliente);
			System.out.println("O cadastro foi realizado com sucesso!");
			return true;
		} catch (Exception e) {
			System.out.println("Erro!! Tente novamente!");

		}

		gravaClinte(cliente);
		return true;
	}

	@SuppressWarnings("resource")
	public  Cliente pesquisarCliente() {
		Scanner read = new Scanner(System.in);
		Integer cpf;
		String linha = "";

		System.out.println("Entre com o cpf do cliente a ser pesquizado:\n");
		cpf = read.nextInt();

		try {
			FileReader fileReader = new FileReader(ARQUIVO_USUARIO);
			BufferedReader lerArq = new BufferedReader(fileReader);
			linha = lerArq.readLine();

			while (linha != null) {
				if (linha.substring(20, 40).trim().equals(cpf.toString())) {
					System.out.println(linha);
					return criaObjeto(linha);
				}

				linha = lerArq.readLine();
			}
			lerArq.close();
			fileReader.close();
		} catch (Exception e) {
}

		return criaObjeto(linha);

	}
	
//---------------------------------------SOBRECARGA DE M�TODO-------------------------------------------------	
	@SuppressWarnings({ "resource", "unused" })
	public  Boolean pesquisarCliente(Integer cpf, Integer senha) {
		Cliente cliente = new Cliente(null, null);
		
		String linha = "";		

		try {
			FileReader fileReader = new FileReader(ARQUIVO_USUARIO);
			BufferedReader lerArq = new BufferedReader(fileReader);
			linha = lerArq.readLine();

			while (linha != null) {
				if ((linha.substring(20, 40).trim().equals(cpf.toString())) && (linha.substring(120, 140).trim().equals(senha.toString()))) {
					System.out.println(linha);
					return true;
				}

				linha = lerArq.readLine();
			}
			lerArq.close();
			fileReader.close();
		} catch (Exception e) {
}

		return false;

	}

	public void listarClientes() {

		try {
			// Indicamos o arquivo que ser� lido
			FileReader fileReader = new FileReader(ARQUIVO_USUARIO);

			// Criamos o objeto bufferReader que nos oferece o m�todo de leitura
			// readLine()

			BufferedReader bufferedReader = new BufferedReader(fileReader);

			// String que ir� receber cada linha do arquivo
			String linha = "";

			while ((linha = bufferedReader.readLine()) != null) {
				// Aqui imprimimos a linha
				System.out.println(linha);
			}

			// liberamos o fluxo dos objetos
			// ou fechamos o arquivo
			fileReader.close();
			bufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("resource")
	public boolean deletarClientes() {
		Scanner read = new Scanner(System.in);
		Integer cpf;
		String linha = "";

		System.out.println("Entre com o cpf do cliente a ser exclu�do:\n");
		cpf = read.nextInt();
		boolean deletou = false;

		// File TEMPORARIO = new
		// File("C:\\Users\\Luiz\\Desktop\\trabalho\\temporario.txt");
		try {
			TEMPORARIO_CLIENTES.createNewFile();
		} catch (IOException e1) {
			System.out.println("N�o foi poss�vel criar o arquivo!");
			e1.printStackTrace();
		}

		try {

			BufferedReader readFile = new BufferedReader(new FileReader(ARQUIVO_USUARIO));
			PrintWriter writer = new PrintWriter(new FileWriter(TEMPORARIO_CLIENTES));
			
			linha = readFile.readLine();
			while ((linha != null)) {
				if (linha.substring(20, 40).trim().equals(cpf.toString())) {
					System.out.println("O cliente foi removido com sucesso da base de dados!");
					deletou = true;

				} else {
					writer.println(linha);
				}
				linha = readFile.readLine();
			}
			if (deletou != true && (linha == null)) {
				System.out.println("O cliente a ser exclu�do n�o existe na base de dados!");
				writer.close();
				readFile.close();
				TEMPORARIO_CLIENTES.delete();
				return false;
			} else
				writer.close();
				readFile.close();				
				ARQUIVO_USUARIO.delete();
				TEMPORARIO_CLIENTES.renameTo(ARQUIVO_USUARIO);
			return true;

		} catch (Exception e) {
			System.out.println("ERRO: N�o foi poss�vel deletar a informa��o!");
		}
		return true;
	}

	@SuppressWarnings("resource")
	public boolean editarCliente() {

		Cliente cliente = new Cliente(nome, cpf, rg, telefone, email, endereco, senha);
		Scanner read = new Scanner(System.in);
		Integer cpf;
		String linha = "";

		System.out.println("Entre com o cpf do cliente a ser editado:\n");
		cpf = read.nextInt();
		boolean editou = false;

		// File TEMPORARIO = new
		// File("C:\\Users\\Luiz\\Desktop\\trabalho\\temporario.txt");
		try {
			TEMPORARIO_CLIENTES.createNewFile();
		} catch (IOException e1) {
			System.out.println("N�o foi poss�vel criar o arquivo!");
			e1.printStackTrace();
		}

		try {

			BufferedReader readFile = new BufferedReader(new FileReader(ARQUIVO_USUARIO));
			PrintWriter writer = new PrintWriter(new FileWriter(TEMPORARIO_CLIENTES));
			linha = readFile.readLine();

			while (linha != null) {
				if (linha.substring(20, 40).trim().equals(cpf.toString())) {

					System.out.println("Nome da cliente: ");
					String nome = read.next();
					cliente.setNome(nome);					
					
					cliente.setCpf(cpf);

					System.out.println("RG: ");
					String rg = read.next();
					cliente.setRg(rg);		

					System.out.println("Telefone: ");
					String telefone = read.next();
					cliente.setTelefone(telefone);

					System.out.println("Email: ");
					String email = read.next();
					cliente.setEmail(email);
					
					System.out.println("Endere�o: ");
					String endereco = read.next();
					cliente.setEmail(endereco);
					
					System.out.println("Senha: ");
					Integer senha = read.nextInt();
					cliente.setSenha(senha);
					// gravaArquivoTemporario(aviao);

					System.out.println("O elemento foi editado com sucesso!");
					editou = true;

				} else {
					writer.println(linha);
				}
				linha = readFile.readLine();
			}
			if (editou != true && (linha == null)) {
				System.out.println("O elemento a ser editado n�o existe!");
				writer.close();
				readFile.close();
				TEMPORARIO_CLIENTES.delete();
				return false;
			} else
				writer.close();
			readFile.close();
			gravaArquivoTemporarioClinte(cliente);
			ARQUIVO_USUARIO.delete();
			TEMPORARIO_CLIENTES.renameTo(ARQUIVO_USUARIO);
			return true;

		} catch (Exception e) {
			System.out.println("ERRO: N�o foi poss�vel editar a informa��o!");
		}
		return true;

	}

}

package br.com.companhiaAerea.models;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import br.com.companhiaAerea.main.CriaAviao;
import br.com.companhiaAerea.main.Principal;
import br.com.companhiaAerea.main.Salvar;



public class Aviao implements CriaAviao, Salvar {
	
	private Integer idAviao;
	private String nome;
	private String modelo;
	private Integer numeroLugares;
	private String registro;
	
    final File ARQUIVO = new File(Principal.ARQUIVO_AVIOES);
    final File TEMPORARIO = new File(Principal.ARQ_TEMP_AVIOES);
    
	public Integer getIdAviao() {
		return idAviao;
	}
	public void setIdAviao(Integer idAviao) {
		this.idAviao = idAviao;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public Integer getNumeroLugares() {
		return numeroLugares;
	}
	public void setNumeroLugares(Integer numeroLugares) {
		this.numeroLugares = numeroLugares;
	}
	public String getRegistro() {
		return registro;
	}
	public void setRegistro(String registro) {
		this.registro = registro;
	}
	
/*
 * 	Construtor
 */
	public Aviao(Integer id, String nome, String modelo, int numero){
		this.idAviao = id;
		this.nome = nome;
		this.modelo = modelo;
		this.numeroLugares = numero;
	}	
/*
 * M�todo para gravar o objeto no arquivo	
 */
	 private void gravaAviao(Aviao aviao) {
	        StringBuilder sb = new StringBuilder();
	        sb.append(completaEspacos(aviao.idAviao.toString(),20));
	        sb.append(completaEspacos(aviao.nome,20));
	        sb.append(completaEspacos(aviao.modelo,20));
	        sb.append(completaEspacos(aviao.numeroLugares.toString(),20));
	        try {
	            FileWriter f = new FileWriter(ARQUIVO,true);
	            f.append(sb.toString()+"\r\n");
	            f.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	 
	 private void gravaArquivoTemporario(Aviao aviao) {
	        StringBuilder sb = new StringBuilder();
	        sb.append(completaEspacos(aviao.idAviao.toString(),20));
	        sb.append(completaEspacos(aviao.nome,20));
	        sb.append(completaEspacos(aviao.modelo,20));
	        sb.append(completaEspacos(aviao.numeroLugares.toString(),20));
	        try {
	            FileWriter f = new FileWriter(TEMPORARIO,true);
	            f.append(sb.toString()+"\r\n");
	            f.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	 
	 private String completaEspacos(String valor, int quantidade){
	        if(valor.length() < quantidade){
	            int qtdFalta = Math.abs(valor.length() - quantidade);
	            for (int i = 0; i < qtdFalta; i++) {
	                valor += " ";
	            }
	        }
	        return valor;
	    }
	 
	
	private Aviao criaObjeto(String linha){
	        Aviao aviao = new Aviao(Integer.parseInt(linha.substring(0, 20).trim()), 
	                linha.substring(20, 40),
	                linha.substring(40, 60),
	        		Integer.parseInt(linha.substring(60, 80).trim()));
	        return aviao;
	    }
	 
	@SuppressWarnings("resource")
	public boolean salvar() {
		String linha = "";
		if (!ARQUIVO.exists())
			try {
				ARQUIVO.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		Aviao aviao = new Aviao(idAviao, nome, modelo, numeroLugares);

		Scanner read = new Scanner(System.in);

		System.out.println("Informe os dados abaixo: ");

		System.out.println("Id do Avi�o: ");
		Integer id = read.nextInt();
		aviao.setIdAviao(id);

		System.out.println("Fabricante do Avi�o: ");
		String fabricante = read.next();
		aviao.setNome(fabricante);

		System.out.println("Modelo: ");
		String modelo = read.next();
		aviao.setModelo(modelo);

		System.out.println("Numero de assentos do Avi�o: ");
		int numeroLugares = read.nextInt();
		aviao.setNumeroLugares(numeroLugares);
		try {

			FileReader fileReader = new FileReader(ARQUIVO);
			BufferedReader lerArq = new BufferedReader(fileReader);
			linha = lerArq.readLine();
			while (linha != null) {
				if (linha.substring(0, 20).trim().equals(id.toString())) {
					System.out.println("Este avi�o j� esta cadastrado!");
					return false;
				} else {
					// System.out.println(aviao + "\n\n");
					linha = lerArq.readLine();
				}

			}
			
			gravaAviao(aviao);
			System.out.println("O avi�o foi cadastrado com sucesso!");
			return true;
		} 
		catch (Exception e) 
		{
			System.out.println("Erro!! Tente novamente!");
			
			
		}

		gravaAviao(aviao);
		return true;
	}
	 
	
	@SuppressWarnings("resource")
	public Aviao pesquisarAviao() {
		Scanner read = new Scanner(System.in);
		Integer id;
		String linha = "";
		
		System.out.println("Entre com o id do avi�o:\n");
		id = read.nextInt();
		
		 try {
	            FileReader fileReader = new FileReader(ARQUIVO);
	            BufferedReader lerArq = new BufferedReader(fileReader);
	            linha = lerArq.readLine();
	            
	            while (linha != null)
	            {	            	
	                if(linha.substring(0,20).trim().equals(id.toString()))
	                {
	                	 System.out.println(linha);
	                    return criaObjeto(linha);
	                }
	                
	                linha = lerArq.readLine();
	            }
	            lerArq.close();
	            fileReader.close();
		 	}
		 catch (Exception e)
		 {
	            e.printStackTrace();
	     }
		
	     return criaObjeto(linha);
	        	
	}
	
	
	public void listarAvioes() {
	
	    try {
	        //Indicamos o arquivo que ser� lido
	        FileReader fileReader = new FileReader(ARQUIVO);
	 
	        //Criamos o objeto bufferReader que nos oferece o m�todo de leitura readLine()
	        
	        BufferedReader bufferedReader = 
	            new BufferedReader(fileReader);
	 
	        //String que ir� receber cada linha do arquivo
	        String linha = "";
	 
	       
	        while ( ( linha = bufferedReader.readLine() ) != null) {
	            //Aqui imprimimos a linha
	            System.out.println(linha);
	        }
	 
	        //liberamos o fluxo dos objetos 
	        // ou fechamos o arquivo
	        fileReader.close();
	        bufferedReader.close();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}
	
	@SuppressWarnings("resource")
	public boolean deletar() {
		Scanner read = new Scanner(System.in);
		Integer id;
		String linha = "";

		System.out.println("Entre com o id do avi�o a ser exclu�do:\n");
		id = read.nextInt();
		boolean deletou = false;		
		
	
		try {
			TEMPORARIO.createNewFile();
		} catch (IOException e1) {
			System.out.println("N�o foi poss�vel criar o arquivo!");
			e1.printStackTrace();
		}

		try {

			BufferedReader readFile = new BufferedReader(new FileReader(ARQUIVO));
			PrintWriter writer = new PrintWriter(new FileWriter(TEMPORARIO));
			linha = readFile.readLine();
			while ((linha != null)) {
				if (linha.substring(0, 20).trim().equals(id.toString())) {
					System.out.println("O elemento foi removido com sucesso!");
					deletou = true;
					
				} else {
					writer.println(linha);
				}				
				linha = readFile.readLine();
			}
			if (deletou != true && (linha == null)) {
				System.out.println("O elemento a ser exclu�do n�o existe!");
				writer.close();
				readFile.close();
				TEMPORARIO.delete();
				return false;
			}
			else
				writer.close();
				readFile.close();
				ARQUIVO.delete();
				TEMPORARIO.renameTo(ARQUIVO);
				return true;
			
		} catch (Exception e) {
			System.out.println("ERRO: N�o foi poss�vel deletar a informa��o!");
		}
		return true;
	}
	
	@SuppressWarnings("resource")
	public boolean editarAviao() {
		
		Aviao aviao = new Aviao(idAviao, nome, modelo, numeroLugares);
		Scanner read = new Scanner(System.in);
		Integer id;
		String linha = "";
		
		System.out.println("Entre com o id do avi�o a ser editado:\n");
		id = read.nextInt();
		boolean editou = false;
		
	
		try {
			TEMPORARIO.createNewFile();
		} catch (IOException e1) {
			System.out.println("N�o foi poss�vel criar o arquivo!");
			e1.printStackTrace();
		}

		try {

			BufferedReader readFile = new BufferedReader(new FileReader(ARQUIVO));
			PrintWriter writer = new PrintWriter(new FileWriter(TEMPORARIO));
			linha = readFile.readLine(); 
			
			while (linha != null) {
				if (linha.substring(0, 10).trim().equals(id.toString())) {

					
					aviao.setIdAviao(id);

					System.out.println("Entre com o fabricante do Avi�o!\n");
					String fabricante = read.next();
					aviao.setNome(fabricante);

					System.out.println("Entre com o modelo do Avi�o!\n");
					String modelo = read.next();
					aviao.setModelo(modelo);

					System.out
							.println("Entre com o numero de assentos do Avi�o!\n");
					int numeroLugares = read.nextInt();
					aviao.setNumeroLugares(numeroLugares);

					
	//				gravaArquivoTemporario(aviao);

					System.out.println("O elemento foi editado com sucesso!");
					editou = true;

				} else {
					writer.println(linha);
				}
				linha = readFile.readLine();
			}
			if (editou != true && (linha == null)) {
				System.out.println("O elemento a ser editado n�o existe!");
				writer.close();
				readFile.close();
				TEMPORARIO.delete();
				return false;
			} else
				writer.close();
				readFile.close();
				gravaArquivoTemporario(aviao);
				ARQUIVO.delete();
				TEMPORARIO.renameTo(ARQUIVO);
				return true;

		} catch (Exception e) {
			System.out.println("ERRO: N�o foi poss�vel editar a informa��o!");
		}
		return true;

	}
	
/*	public boolean ordenarLista() throws IOException{
		  
        FileReader fileReader = new FileReader(ARQUIVO);        
        BufferedReader bufferedReader =  new BufferedReader(fileReader);       
        String linha = "";
        TEMPORARIO.createNewFile();
        ArrayList<Object> aviao = new ArrayList<>();
        String c;
      
       
        try {
        	BufferedReader readFile = new BufferedReader(new FileReader(ARQUIVO));
			PrintWriter writer = new PrintWriter(new FileWriter(TEMPORARIO));
			linha = readFile.readLine(); 
			
        	
			while (linha != null) {
				 	
				 aviao.add(linha.substring(0, 20).trim());
				String L = (linha.substring(0, 20).trim());
				 Collections.sort(aviao, L.);
				 linha = readFile.readLine();
//			    System.out.println(linha);
				
					 
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}    */
}

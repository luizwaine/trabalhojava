package br.com.companhiaAerea.models;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import br.com.companhiaAerea.main.Pessoa;
import br.com.companhiaAerea.main.Principal;

//M�TODO ABSTRATO QUE EXTENDS CONSTRUTORES PARA AS CLASSES CLIENTE E USU�RIO
public abstract class Pessoas implements Pessoa {

	protected static String nome;
	protected static Integer cpf;
	protected static String rg;
	protected static String telefone;
	protected static String email;
	protected static String endereco;
	
	final File ARQ_PESSOAS = new File(Principal.ARQUIVO_PESSOAS);
    final File TEMPORARIO_PESSOAS = new File(Principal.ARQ_TEMP_PESSOAS);

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		Pessoas.nome = nome;
	}

	public Integer getCpf() {
		return cpf;
	}

	public void setCpf(Integer cpf) {
		Pessoas.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		Pessoas.rg = rg;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		Pessoas.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		Pessoas.email = email;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		Pessoas.endereco = endereco;
	}	

	/*
	 * Construtor
	 */
	public Pessoas(String nome, Integer cpf, String rg, String telefone, String email, String endereco) {
		super();
		Pessoas.nome = nome;
		Pessoas.cpf = cpf;
		Pessoas.rg = rg;
		Pessoas.telefone = telefone;
		Pessoas.email = email;
		Pessoas.endereco = endereco;
	}

	/*
	 * M�todo para gravar o objeto no arquivo
	 */
	protected void gravaPessoa(Pessoas pessoa) {
		StringBuilder sb = new StringBuilder();
		sb.append(completaEspacos(Pessoas.nome, 20));
		sb.append(completaEspacos(Pessoas.cpf.toString(), 20));
		sb.append(completaEspacos(Pessoas.rg, 20));
		sb.append(completaEspacos(Pessoas.telefone, 20));
		sb.append(completaEspacos(Pessoas.email, 20));
		sb.append(completaEspacos(Pessoas.endereco, 20));
		try {
			FileWriter f = new FileWriter(ARQ_PESSOAS, true);
			f.append(sb.toString() + "\r\n");
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void gravaArquivoTemporario(Pessoas pessoa) {
		StringBuilder sb = new StringBuilder();
		sb.append(completaEspacos(Pessoas.nome, 20));
		sb.append(completaEspacos(Pessoas.cpf.toString(), 20));
		sb.append(completaEspacos(Pessoas.rg, 20));
		sb.append(completaEspacos(Pessoas.telefone, 20));
		sb.append(completaEspacos(Pessoas.email, 20));
		sb.append(completaEspacos(Pessoas.endereco, 20));
		try {
			FileWriter f = new FileWriter(TEMPORARIO_PESSOAS, true);
			f.append(sb.toString() + "\r\n");
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected String completaEspacos(String valor, int quantidade) {
		if (valor.length() < quantidade) {
			int qtdFalta = Math.abs(valor.length() - quantidade);
			for (int i = 0; i < qtdFalta; i++) {
				valor += " ";
			}
		}
		return valor;
	}

	

	@SuppressWarnings("resource")
	public boolean salvar(Pessoas pessoa) {
		String linha = "";
		if (!ARQ_PESSOAS.exists())
			try {
				ARQ_PESSOAS.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		

		Scanner read = new Scanner(System.in);

		System.out.println("Informe os dados da abaixo: ");

		System.out.println("Nome da pessoa: ");
		String nome = read.next();
		pessoa.setNome(nome);

		System.out.println("CPF: ");
		Integer cpf = read.nextInt();
		pessoa.setCpf(cpf);

		System.out.println("RG: ");
		String rg = read.next();
		pessoa.setRg(rg);		

		System.out.println("Telefone: ");
		String telefone = read.next();
		pessoa.setTelefone(telefone);

		System.out.println("Email: ");
		String email = read.next();
		pessoa.setEmail(email);
		
		System.out.println("Endere�o: ");
		String endereco = read.next();
		pessoa.setEndereco(endereco);
		
		try {

			FileReader fileReader = new FileReader(ARQ_PESSOAS);
			BufferedReader lerArq = new BufferedReader(fileReader);
			linha = lerArq.readLine();
			while (linha != null) {
				if (linha.substring(20, 40).trim().equals(cpf.toString())) {
					System.out.println("Esta pessoa j� esta cadastrada!");
					return false;
				} else {
					// System.out.println(aviao + "\n\n");
					linha = lerArq.readLine();
				}

			}

			gravaPessoa(pessoa);
			System.out.println("O cadastro foi realizado com sucesso!");
			return true;
		} catch (Exception e) {
			System.out.println("Erro!! Tente novamente!");

		}

		gravaPessoa(pessoa);
		return true;
	}



	public void listarPessoas() {

		try {
			// Indicamos o arquivo que ser� lido
			FileReader fileReader = new FileReader(ARQ_PESSOAS);

			// Criamos o objeto bufferReader que nos oferece o m�todo de leitura
			// readLine()

			BufferedReader bufferedReader = new BufferedReader(fileReader);

			// String que ir� receber cada linha do arquivo
			String linha = "";

			while ((linha = bufferedReader.readLine()) != null) {
				// Aqui imprimimos a linha
				System.out.println(linha);
			}

			// liberamos o fluxo dos objetos
			// ou fechamos o arquivo
			fileReader.close();
			bufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("resource")
	public boolean deletar() {
		Scanner read = new Scanner(System.in);
		Integer cpf;
		String linha = "";

		System.out.println("Entre com o cpf da pessoa a ser exclu�da:\n");
		cpf = read.nextInt();
		boolean deletou = false;

		// File TEMPORARIO = new
		// File("C:\\Users\\Luiz\\Desktop\\trabalho\\temporario.txt");
		try {
			TEMPORARIO_PESSOAS.createNewFile();
		} catch (IOException e1) {
			System.out.println("N�o foi poss�vel criar o arquivo!");
			e1.printStackTrace();
		}

		try {

			BufferedReader readFile = new BufferedReader(new FileReader(ARQ_PESSOAS));
			PrintWriter writer = new PrintWriter(new FileWriter(TEMPORARIO_PESSOAS));
			linha = readFile.readLine();
			while ((linha != null && linha !="")) {
				if (linha.substring(20, 40).trim().equals(cpf.toString())) {
					System.out.println("O elemento foi removido com sucesso!");
					deletou = true;

				} else {
					writer.println(linha);
				}
				linha = readFile.readLine();
			}
			if (deletou != true && (linha == null)) {
				System.out.println("O elemento a ser exclu�do n�o existe!");
				writer.close();
				readFile.close();
				TEMPORARIO_PESSOAS.delete();
				return false;
			} else
				writer.close();
			readFile.close();
			ARQ_PESSOAS.delete();
			TEMPORARIO_PESSOAS.renameTo(ARQ_PESSOAS);
			return true;

		} catch (Exception e) {
			System.out.println("ERRO: N�o foi poss�vel deletar a informa��o!");
		}
		return true;
	}



}

package br.com.companhiaAerea.models;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import br.com.companhiaAerea.main.Principal;

public class Voo {
	
	private Integer idVoo;
	private String cidadeOrigem;
	private String cidadeDestino;
	private Float valor;
	private String horaSaida;
	private String horaChegada;
	private String dataVoo;
	
	
	final File ARQUIVO_VOO = new File(Principal.ARQUIVO_VOOS);
	final File TEMPORARIO_VOO = new File(Principal.ARQ_TEMP_VOOS);
	
	
	public Integer getIdVoo() {
		return idVoo;
	}
	public void setIdVoo(Integer idVoo) {
		this.idVoo = idVoo;
	}
	public String getCidadeOrigem() {
		return cidadeOrigem;
	}
	public void setCidadeOrigem(String cidadeOrigem) {
		this.cidadeOrigem = cidadeOrigem;
	}
	public String getCidadeDestino() {
		return cidadeDestino;
	}
	public void setCidadeDestino(String cidadeDestino) {
		this.cidadeDestino = cidadeDestino;
	}
	public Float getValor() {
		return valor;
	}
	public void setValor(Float valor) {
		this.valor = valor;
	}
	public String getHoraSaida() {
		return horaSaida;
	}
	public void setHoraSaida(String horaSaida) {
		this.horaSaida = horaSaida;
	}
	public String getDataVoo() {
		return dataVoo;
	}
	public void setDataVoo(String dataVoo) {
		this.dataVoo = dataVoo;
	}
	public String getHoraChegada() {
		return horaChegada;
	}
	public void setHoraChegada(String horaChegada) {
		this.horaChegada = horaChegada;
	}
	
	public Boolean validaVoo(){
		
		if(this.idVoo == null || this.idVoo < 0)
			return false;
		if(this.cidadeDestino.equals(null) || this.cidadeDestino.equals(""))
			return false;
		if(this.cidadeDestino.equals(null) || this.cidadeDestino.equals(""))
			return false;
		if(this.cidadeOrigem.equals(null) || this.cidadeOrigem.equals(""))
			return false;
		if(this.dataVoo.equals(null) || this.dataVoo.equals(""))
			return false;
		if(this.horaChegada.equals(null) || this.horaChegada.equals(""))
			return false;
		if(this.horaSaida.equals(null) || this.horaSaida.equals(""))
			return false;
		return true;
		
	}
	
	public Voo(Integer idVoo, String cidadeOrigem, String cidadeDestino, Float valor, String horaSaida, String horaChegada, String dataVoo) {

		this.idVoo = idVoo;
		this.cidadeOrigem = cidadeOrigem;
		this.cidadeDestino = cidadeDestino;
		this.valor = valor;
		this.horaSaida = horaSaida;
		this.horaChegada = horaChegada;
		this.dataVoo = dataVoo;

	}

//	public void Exibe() {
//		System.out.println("Nome da cidade: " + getNome());
//		System.out.println("Nome do estado: " + getEstado());
//		System.out.println("Nome do pa�s: " + getPais());
//	}

	private void gravaVoo(Voo voo) {
		StringBuilder sb = new StringBuilder();
		sb.append(completaEspacos(voo.idVoo.toString(), 20));
		sb.append(completaEspacos(voo.cidadeOrigem, 20));
		sb.append(completaEspacos(voo.cidadeDestino, 20));
		sb.append(completaEspacos(voo.valor.toString(), 20));
		sb.append(completaEspacos(voo.horaSaida, 20));
		sb.append(completaEspacos(voo.horaChegada, 20));		
		sb.append(completaEspacos(voo.dataVoo, 20));
		try {
			FileWriter f = new FileWriter(ARQUIVO_VOO, true);
			f.append(sb.toString() + "\r\n");
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String completaEspacos(String valor, int quantidade) {
		if (valor.length() < quantidade) {
			int qtdFalta = Math.abs(valor.length() - quantidade);
			for (int i = 0; i < qtdFalta; i++) {
				valor += " ";
			}
		}
		return valor;
	}

	private Voo criaObjeto(String linha) {
		
		Voo voo = new Voo(Integer.parseInt(linha.substring(0, 20).trim()),
				linha.substring(20, 40),
				linha.substring(40, 60),
				Float.parseFloat(linha.substring(60, 80).trim()),
				linha.substring(80, 100),
				linha.substring(100, 120),
				linha.substring(120, 140));
		
		return voo;
	}

	private void gravaArquivoTemporario(Voo voo) {
		StringBuilder sb = new StringBuilder();
		sb.append(completaEspacos(voo.idVoo.toString(), 20));
		sb.append(completaEspacos(voo.cidadeOrigem, 20));
		sb.append(completaEspacos(voo.cidadeDestino, 20));
		sb.append(completaEspacos(voo.valor.toString(), 20));
		sb.append(completaEspacos(voo.horaSaida, 20));
		sb.append(completaEspacos(voo.horaChegada, 20));
		sb.append(completaEspacos(voo.dataVoo, 20));
		
		try {
			FileWriter f = new FileWriter(TEMPORARIO_VOO, true);
			f.append(sb.toString() + "\r\n");
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("resource")
	public boolean salvarVoo() throws IOException {
		
		Cidade cidade = new Cidade(null, null, null, null);
		String linha = "";
		if (!ARQUIVO_VOO.exists())
			ARQUIVO_VOO.createNewFile();

		Voo voo = new Voo(idVoo, cidadeOrigem, cidadeDestino, valor, horaSaida, horaChegada, dataVoo);

		Scanner read = new Scanner(System.in);

		System.out.println("Informe os dados abaixo: ");

		System.out.println("Id do Voo: ");
		Integer id = read.nextInt();
		voo.setIdVoo(id);

//------------------------VERIFICA SE A CIDADE DE ORIGEM EST� CADASTRADA NA BASE DE DADOS--------------------------------------
		System.out.println("Cidade de Origem do v�o: ");
		String cidadeOrigem = read.next();
		
			if(cidade.pesquisarCidade(cidadeOrigem) == true){
				voo.setCidadeOrigem(cidadeOrigem);
			}
			else{				
			System.out.println("Esta cidade n�o est� cadastrada na base de dados!");
			return false;
			}
			
			
//------------------------VERIFICA SE A CIDADE DE DESTINO EST� CADASTRADA NA BASE DE DADOS-------------------------------------
		System.out.println("Cidade de Destino do v�o: ");
		String cidadeDestino = read.next();
			if(cidade.pesquisarCidade(cidadeDestino) == true){
				voo.setCidadeDestino(cidadeDestino);
			}
			else{
				System.out.println("Esta cidade n�o est� cadastrada na base de dados!");
				return false;
			}
		

		System.out.println("Valor do v�o: ");
		Float valor = read.nextFloat();
		voo.setValor(valor);
		
		System.out.println("Hora de sa�da do v�o: ");
		String horaSaida = read.next();
		voo.setHoraSaida(horaSaida);

		System.out.println("Hora de chegada ao destino: ");
		String horaChegada = read.next();
		voo.setHoraChegada(horaChegada);

		System.out.println("Data do v�o: ");
		String dataVoo = read.next();
		voo.setDataVoo(dataVoo);
		try {

			FileReader fileReader = new FileReader(ARQUIVO_VOO);
			BufferedReader lerArq = new BufferedReader(fileReader);
			linha = lerArq.readLine();
			while (linha != null) {
				if (linha.substring(0, 20).trim().equals(id.toString())) {
					System.out.println("Este v�o j� esta cadastrado!");
					return false;
				} else {
					// System.out.println(aviao + "\n\n");
					linha = lerArq.readLine();
				}

			}

			gravaVoo(voo);
			System.out.println("A cidade foi cadastrada com sucesso!");
			return true;
		} catch (Exception e) {
			System.out.println("Erro!! Tente novamente!");

		}

		gravaVoo(voo);
		return true;
	}

	@SuppressWarnings("resource")
	public Voo pesquisarVoo() throws IOException {
		Scanner read = new Scanner(System.in);
		Integer id;
		String linha = "";

		System.out.println("Entre com o id da cidade:\n");
		id = read.nextInt();

		try {
			FileReader fileReader = new FileReader(ARQUIVO_VOO);
			BufferedReader lerArq = new BufferedReader(fileReader);
			linha = lerArq.readLine();

			while (linha != null) {
				if (linha.substring(0, 20).trim().equals(id.toString())) {
					System.out.println(linha);
					return criaObjeto(linha);
				}

				linha = lerArq.readLine();
			}
			lerArq.close();
			fileReader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return criaObjeto(linha);

	}
	
//-------------------------	SOBRECARGA DE M�TODO-----------------------------------
	@SuppressWarnings("resource")
	public Boolean pesquisarVoo(String cidadeOrigem, String cidadeDestino) throws IOException {		
	
		String linha = "";		

		try {
			FileReader fileReader = new FileReader(ARQUIVO_VOO);
			BufferedReader lerArq = new BufferedReader(fileReader);
			linha = lerArq.readLine();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy       hh:mm:ss");  
		 

			while (linha != null) {
				if (linha.substring(20, 40).trim().equals(cidadeOrigem) && linha.substring(40, 60).trim().equals(cidadeDestino)) {
					
					System.out.println("\n\n----------------------BILHETE A�REO------------------------");
					System.out.println("\n\nid: "+ linha.substring(0, 20).trim());
					System.out.println("cidade Origem: "+  linha.substring(20, 40).trim());
					System.out.println("Cidade Destino: "+  linha.substring(40, 60).trim());
					System.out.println("Hora de Sa�da: "+  linha.substring(80, 100).trim());
					System.out.println("Valor do V�o: R$ "+  linha.substring(60, 80).trim());
					System.out.println("Data e hora da compra: "+sdf.format(new Date()));
					
					return true ;
				}

				linha = lerArq.readLine();
			}
			lerArq.close();
			fileReader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("\n\nDesculpe! A Companhia n�o faz a rota informada!");
		return false;

	}

	public void listarVoos() {

		try {
			// Indicamos o arquivo que ser� lido
			FileReader fileReader = new FileReader(ARQUIVO_VOO);

			// Criamos o objeto bufferReader que nos oferece o m�todo de leitura
			// readLine()

			BufferedReader bufferedReader = new BufferedReader(fileReader);

			// String que ir� receber cada linha do arquivo
			String linha = "";

			while ((linha = bufferedReader.readLine()) != null) {
				// Aqui imprimimos a linha
				System.out.println(linha);
			}

			// liberamos o fluxo dos objetos
			// ou fechamos o arquivo
			fileReader.close();
			bufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("resource")
	public boolean deletar() throws IOException {
		Scanner read = new Scanner(System.in);
		Integer id;
		String linha = "";

		System.out.println("Entre com o id do voo a ser exclu�do:\n");
		id = read.nextInt();
		boolean deletou = false;

		// File TEMPORARIO = new
		// File("C:\\Users\\Luiz\\Desktop\\trabalho\\temporario.txt");
		TEMPORARIO_VOO.createNewFile();

		try {

			BufferedReader readFile = new BufferedReader(new FileReader(
					ARQUIVO_VOO));
			PrintWriter writer = new PrintWriter(new FileWriter(
					TEMPORARIO_VOO));
			linha = readFile.readLine();
			while ((linha != null)) {
				if (linha.substring(0, 20).trim().equals(id.toString())) {
					System.out.println("O voo foi exclu�do com sucesso!");
					deletou = true;

				} else {
					writer.println(linha);
				}
				linha = readFile.readLine();
			}
			if (deletou != true && (linha == null)) {
				System.out.println("O elemento a ser exclu�do n�o existe!");
				writer.close();
				readFile.close();
				TEMPORARIO_VOO.delete();
				return false;
			} else
				writer.close();
			readFile.close();
			ARQUIVO_VOO.delete();
			TEMPORARIO_VOO.renameTo(ARQUIVO_VOO);
			return true;

		} catch (Exception e) {
			System.out.println("ERRO: N�o foi poss�vel deletar a informa��o!");
		}
		return true;
	}

	@SuppressWarnings("resource")
	public boolean editarVoo() throws IOException {

		Voo voo = new Voo(idVoo, cidadeOrigem, cidadeDestino, valor, horaSaida, horaChegada, dataVoo);
		Scanner read = new Scanner(System.in);
		Integer id;
		String linha = "";

		System.out.println("Entre com o id do voo a ser editado:\n");
		id = read.nextInt();
		boolean editou = false;

		// File TEMPORARIO = new
		// File("C:\\Users\\Luiz\\Desktop\\trabalho\\temporario.txt");
		TEMPORARIO_VOO.createNewFile();

		try {

			BufferedReader readFile = new BufferedReader(new FileReader(
					ARQUIVO_VOO));
			PrintWriter writer = new PrintWriter(new FileWriter(
					TEMPORARIO_VOO));
			linha = readFile.readLine();

			while (linha != null) {
				if (linha.substring(0, 20).trim().equals(id.toString())) {

					voo.setIdVoo(id);

					System.out.println("Cidade de Origem do v�o: ");
					String cidadeOrigem = read.next();
					voo.setCidadeOrigem(cidadeOrigem);

					System.out.println("Cidade de Destino do v�o: ");
					String cidadeDestino = read.next();
					voo.setCidadeDestino(cidadeDestino);

					System.out.println("Valor do v�o: ");
					Float valor = read.nextFloat();
					voo.setValor(valor);
					
					System.out.println("Hora de sa�da do v�o: ");
					String horaSaida = read.next();
					voo.setHoraSaida(horaSaida);

					System.out.println("Hora de chegada ao destino: ");
					String horaDestino = read.next();
					voo.setCidadeDestino(horaDestino);

					System.out.println("Data do v�o: ");
					String dataVoo = read.next();
					voo.setDataVoo(dataVoo);

					// gravaArquivoTemporario(cidade);

					System.out.println("A informa��o foi editada com sucesso!");
					editou = true;

				} else {
					writer.println(linha);
				}
				linha = readFile.readLine();
			}
			if (editou != true && (linha == null)) {
				System.out.println("A informa��o a ser editada n�o existe!");
				writer.close();
				readFile.close();
				TEMPORARIO_VOO.delete();
				return false;
			} else
				writer.close();
				readFile.close();
				gravaArquivoTemporario(voo);
				ARQUIVO_VOO.delete();
				TEMPORARIO_VOO.renameTo(ARQUIVO_VOO);
				return true;

		} catch (Exception e) {
			System.out.println("ERRO: N�o foi poss�vel editar a informa��o!");
		}
		return true;

	}
	

}

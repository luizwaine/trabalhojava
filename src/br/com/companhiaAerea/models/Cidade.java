package br.com.companhiaAerea.models;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import br.com.companhiaAerea.main.Principal;
import br.com.companhiaAerea.main.Salvar;

public class Cidade implements Salvar{
	

	private Integer id;
	private String nome;
	private String estado;
	private String pais;

	final File ARQUIVO_CIDADE = new File(Principal.ARQUIVO_CIDADES);
	final File TEMPORARIO_CIDADE = new File(Principal.ARQ_TEMP_CIDADES);

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	/**
	 * construtor Principal
	 * 
	 * @param name
	 * @param state
	 * @param country
	 */
	public Cidade(Integer id, String nome, String estado, String pais) {

		this.id = id;
		this.nome = nome;
		this.estado = estado;
		this.pais = pais;

	}

	public void Exibe() {
		System.out.println("Nome da cidade: " + getNome());
		System.out.println("Nome do estado: " + getEstado());
		System.out.println("Nome do pa�s: " + getPais());
	}

	private void gravaCidade(Cidade cidade) {
		StringBuilder sb = new StringBuilder();
		sb.append(completaEspacos(cidade.id.toString(), 20));
		sb.append(completaEspacos(cidade.nome, 20));
		sb.append(completaEspacos(cidade.estado, 20));
		sb.append(completaEspacos(cidade.pais, 20));
		try {
			FileWriter f = new FileWriter(ARQUIVO_CIDADE, true);
			f.append(sb.toString() + "\r\n");
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String completaEspacos(String valor, int quantidade) {
		if (valor.length() < quantidade) {
			int qtdFalta = Math.abs(valor.length() - quantidade);
			for (int i = 0; i < qtdFalta; i++) {
				valor += " ";
			}
		}
		return valor;
	}

	private Cidade criaObjeto(String linha) {
		Cidade cidade = new Cidade(Integer.parseInt(linha.substring(0, 20)
				.trim()), linha.substring(20, 40), linha.substring(40, 60),
				linha.substring(60, 80));
		return cidade;
	}

	private void gravaArquivoTemporario(Cidade cidade) {
		StringBuilder sb = new StringBuilder();
		sb.append(completaEspacos(cidade.id.toString(), 20));
		sb.append(completaEspacos(cidade.nome, 20));
		sb.append(completaEspacos(cidade.estado, 20));
		sb.append(completaEspacos(cidade.pais, 20));
		try {
			FileWriter f = new FileWriter(TEMPORARIO_CIDADE, true);
			f.append(sb.toString() + "\r\n");
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("resource")
	public boolean salvar() throws IOException {
		String linha = "";
		if (!ARQUIVO_CIDADE.exists())
			ARQUIVO_CIDADE.createNewFile();

		Cidade cidade = new Cidade(id, nome, estado, pais);

		Scanner read = new Scanner(System.in);

		System.out.println("Informe os dados abaixo: ");

		System.out.println("Id da Cidade: ");
		Integer id = read.nextInt();
		cidade.setId(id);

		System.out.println("Nome da cidade: ");
		String nome = read.next();
		cidade.setNome(nome);

		System.out.println("Estado: ");
		String estado = read.next();
		cidade.setEstado(estado);

		System.out.println("Pa�s: ");
		String pais = read.next();
		cidade.setPais(pais);
		try {

			FileReader fileReader = new FileReader(ARQUIVO_CIDADE);
			BufferedReader lerArq = new BufferedReader(fileReader);
			linha = lerArq.readLine();
			while (linha != null && !linha.equals("")) {
				if (linha.substring(0, 20).trim().equals(id.toString())) {
					System.out.println("Esta cidade j� esta cadastrada!");
					return false;
				} else {
					
					linha = lerArq.readLine();
				}

			}

			gravaCidade(cidade);
			System.out.println("A cidade foi cadastrada com sucesso!");
			return true;
		} catch (Exception e) {
			System.out.println("Erro!! Tente novamente!");

		}

		gravaCidade(cidade);
		return true;
	}

	@SuppressWarnings("resource")
	public Cidade pesquisarCidade() throws IOException {
		Scanner read = new Scanner(System.in);
		Integer id;
		String linha = "";

		System.out.println("Entre com o id da cidade:\n");
		id = read.nextInt();

		try {
			FileReader fileReader = new FileReader(ARQUIVO_CIDADE);
			BufferedReader lerArq = new BufferedReader(fileReader);
			linha = lerArq.readLine();

			while (linha != null) {
				if (linha.substring(0, 20).trim().equals(id.toString())) {
					System.out.println(linha);
					return criaObjeto(linha);
				}

				linha = lerArq.readLine();
			}
			lerArq.close();
			fileReader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return criaObjeto(linha);

	}
	
	//SOBRECARGA DE METODOS PARA VERIFICAR SE A CIDADE DO V�O EXISTE NA BASE DE DADOS
	@SuppressWarnings("resource")
	public Boolean pesquisarCidade(String nome) throws IOException {
		
		String linha = "";
		try {
			FileReader fileReader = new FileReader(ARQUIVO_CIDADE);
			BufferedReader lerArq = new BufferedReader(fileReader);
			linha = lerArq.readLine();

			while (linha != null) {
				if (linha.substring(20, 40).trim().equals(nome)) {					
					return true;
					
				}

				linha = lerArq.readLine();
			}
			lerArq.close();
			fileReader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;

	}

	public void listarCidades() {

		try {
			// Indicamos o arquivo que ser� lido
			FileReader fileReader = new FileReader(ARQUIVO_CIDADE);

			// Criamos o objeto bufferReader que nos oferece o m�todo de leitura
			// readLine()

			BufferedReader bufferedReader = new BufferedReader(fileReader);

			// String que ir� receber cada linha do arquivo
			String linha = "";

			while ((linha = bufferedReader.readLine()) != null) {
				// Aqui imprimimos a linha
				System.out.println(linha);
			}

			// liberamos o fluxo dos objetos
			// ou fechamos o arquivo
			fileReader.close();
			bufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("resource")
	public boolean deletar() throws IOException {
		Scanner read = new Scanner(System.in);
		Integer id;
		String linha = "";

		System.out.println("Entre com o id da cidade a ser exclu�da:\n");
		id = read.nextInt();
		boolean deletou = false;

	
		TEMPORARIO_CIDADE.createNewFile();

		try {

			BufferedReader readFile = new BufferedReader(new FileReader(
					ARQUIVO_CIDADE));
			PrintWriter writer = new PrintWriter(new FileWriter(
					TEMPORARIO_CIDADE));
			linha = readFile.readLine();
			while ((linha != null && !linha.equals(""))) {
				if (linha.substring(0, 20).trim().equals(id.toString())) {
					System.out.println("A cidade foi exclu�da com sucesso!");
					deletou = true;

				} else {
					writer.println(linha);
				}
				linha = readFile.readLine();
			}
			if (deletou != true && (linha == null)) {
				System.out.println("O elemento a ser exclu�do n�o existe!");
				writer.close();
				readFile.close();
				TEMPORARIO_CIDADE.delete();
				return false;
			} else
				writer.close();
				readFile.close();
				ARQUIVO_CIDADE.delete();
				(TEMPORARIO_CIDADE).renameTo(ARQUIVO_CIDADE);
				return true;

		} catch (Exception e) {
			System.out.println("ERRO: N�o foi poss�vel deletar a informa��o!");
		}
		return true;
	}

	@SuppressWarnings("resource")
	public boolean editarCidade() throws IOException {

		Cidade cidade = new Cidade(id, nome, estado, pais);
		Scanner read = new Scanner(System.in);
		Integer id;
		String linha = "";

		System.out.println("Entre com o id da cidade a ser editada:\n");
		id = read.nextInt();
		boolean editou = false;

		// File TEMPORARIO = new
		// File("C:\\Users\\Luiz\\Desktop\\trabalho\\temporario.txt");
		TEMPORARIO_CIDADE.createNewFile();

		try {

			BufferedReader readFile = new BufferedReader(new FileReader(
					ARQUIVO_CIDADE));
			PrintWriter writer = new PrintWriter(new FileWriter(
					TEMPORARIO_CIDADE));
			linha = readFile.readLine();

			while (linha != null) {
				if (linha.substring(0, 20).trim().equals(id.toString())) {

					cidade.setId(id);

					System.out.println("Nome da cidade: ");
					String nome = read.next();
					cidade.setNome(nome);

					System.out.println("Estado: ");
					String estado = read.next();
					cidade.setEstado(estado);

					System.out.println("Pa�s: ");
					String pais = read.next();
					cidade.setPais(pais);

					// gravaArquivoTemporario(cidade);

					System.out.println("A informa��o foi editada com sucesso!");
					editou = true;

				} else {
					writer.println(linha);
				}
				linha = readFile.readLine();
			}
			if (editou != true && (linha == null)) {
				System.out.println("A informa��o a ser editada n�o existe!");
				writer.close();
				readFile.close();
				TEMPORARIO_CIDADE.delete();
				return false;
			} else
				writer.close();
				readFile.close();
				gravaArquivoTemporario(cidade);
				ARQUIVO_CIDADE.delete();
				TEMPORARIO_CIDADE.renameTo(ARQUIVO_CIDADE);
				return true;

		} catch (Exception e) {
			System.out.println("ERRO: N�o foi poss�vel editar a informa��o!");
		}
		return true;

	}



}

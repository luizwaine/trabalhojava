package br.com.companhiaAerea.models;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import br.com.companhiaAerea.main.Principal;

public class Usuario extends Cliente {
	
	private String matricula;	

	final File ARQUIVO_USUARIO = new File(Principal.ARQUIVO_USUARIOS);
	final File TEMPORARIO_USUARIO = new File(Principal.ARQ_TEMP_USUARIOS);

	public Usuario(String nome, Integer cpf, String rg, String telefone, String email, String endereco, Integer senha, String matricula) {
		super(nome, cpf, rg, telefone, email, endereco, senha);
		
		this.matricula = matricula;
		
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	

//SOBRECARGA DE CONSTRUTOR	
	public Usuario(Integer senha, String matricula) {
		super(nome, cpf, rg, telefone, email, endereco, senha);
		this.matricula = matricula;
		
	}
	
	protected void  gravaUsuario(Usuario usuario){
		StringBuilder sb = new StringBuilder();
		sb.append(completaEspacos(Pessoas.nome, 20));
		sb.append(completaEspacos(Pessoas.cpf.toString(), 20));
		sb.append(completaEspacos(Pessoas.rg, 20));
		sb.append(completaEspacos(Pessoas.telefone, 20));
		sb.append(completaEspacos(Pessoas.email, 20));
		sb.append(completaEspacos(Pessoas.endereco, 20));
		sb.append(completaEspacos(usuario.senha.toString(), 20));
		sb.append(completaEspacos(usuario.matricula, 20));
		
		try {
			FileWriter f = new FileWriter(ARQUIVO_USUARIO, true);
			f.append(sb.toString() + "\r\n");
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
//SOBRECARGA DE CONSTRUTOR	
	protected void  gravaUsuario(Integer senha, String matricula, Usuario usuario){
		StringBuilder sb = new StringBuilder();		
		sb.append(completaEspacos(usuario.senha.toString(), 20));		
		sb.append(completaEspacos(usuario.matricula, 20));
		
		try {
			FileWriter f = new FileWriter(ARQUIVO_USUARIO, true);
			f.append(sb.toString() + "\r\n");
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected void  gravaArquivoTemporarioUsuario(Usuario usuario){
		StringBuilder sb = new StringBuilder();
		sb.append(completaEspacos(Pessoas.nome, 20));
		sb.append(completaEspacos(Pessoas.cpf.toString(), 20));
		sb.append(completaEspacos(Pessoas.rg, 20));
		sb.append(completaEspacos(Pessoas.telefone, 20));
		sb.append(completaEspacos(Pessoas.email, 20));
		sb.append(completaEspacos(Pessoas.endereco, 20));
		sb.append(completaEspacos(usuario.senha.toString(), 20));
		sb.append(completaEspacos(usuario.matricula, 20));;
		
		try {
			FileWriter f = new FileWriter(TEMPORARIO_USUARIO, true);
			f.append(sb.toString() + "\r\n");
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected String completaEspacos(String valor, int quantidade) {
		if (valor.length() < quantidade) {
			int qtdFalta = Math.abs(valor.length() - quantidade);
			for (int i = 0; i < qtdFalta; i++) {
				valor += " ";
			}
		}
		return valor;
	}

	private Usuario criaObjeto(String linha) {
		Usuario usuario = new Usuario(linha.substring(0, 20), 
				Integer.parseInt(linha.substring(20, 40).trim()), 
				linha.substring(40, 60),
				linha.substring(60, 80),
				linha.substring(80, 100),
				linha.substring(100, 120),
				Integer.parseInt(linha.substring(120, 140).trim()),
				linha.substring(140, 160));
		return usuario;
	}
	
//-------------------------------SOBRECARGA DE M�TODOS--------------------------	
	@SuppressWarnings("unused")
	private Usuario criaObjeto(String linha, Usuario usuario) {
		 usuario = new Usuario(
				Integer.parseInt(linha.substring(0, 20).trim()), 				
				linha.substring(20, 40).trim());
		return usuario;
	}

	@SuppressWarnings("resource")
	public boolean salvarUsuarios() {
		String linha = "";
		if (!ARQUIVO_USUARIO.exists())
			try {
				ARQUIVO_USUARIO.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		Usuario usuario = new Usuario(nome, cpf, rg, telefone, email, endereco, senha, matricula);

		Scanner read = new Scanner(System.in);

		System.out.println("Informe os dados da abaixo: ");

		System.out.println("Usu�rio: ");
		String nome = read.next();
		usuario.setNome(nome);

		System.out.println("CPF: ");
		Integer cpf = read.nextInt();
		usuario.setCpf(cpf);

		System.out.println("RG: ");
		String rg = read.next();		
		usuario.setRg(rg);		

		System.out.println("Telefone: ");
		String telefone = read.next();
		usuario.setTelefone(telefone);

		System.out.println("Email: ");
		String email = read.next();
		usuario.setEmail(email);
		
		System.out.println("Endere�o: ");
		String endereco = read.next();
		usuario.setEndereco(endereco);
		
		System.out.println("Senha: ");
		Integer senha = read.nextInt();
		usuario.setSenha(senha);
		
		System.out.println("Matricula: ");
		String matricula = read.next();
		usuario.setMatricula(matricula);
		
		try {

			FileReader fileReader = new FileReader(ARQUIVO_USUARIO);
			BufferedReader lerArq = new BufferedReader(fileReader);
			linha = lerArq.readLine();
			while (linha != null) {
				if (linha.substring(20, 40).trim().equals(cpf.toString())) {
					System.out.println("Este usuario j� esta cadastrado!");
					return false;
				} else {
					// System.out.println(aviao + "\n\n");
					linha = lerArq.readLine();
				}

			}

			gravaUsuario(usuario);
			System.out.println("O cadastro foi realizado com sucesso!");
			return true;
		} catch (Exception e) {
			System.out.println("Erro!! Tente novamente!");

		}

		gravaUsuario(usuario);
		return true;
	}

	@SuppressWarnings("resource")
	public  Usuario pesquisarUsuario() {
		Scanner read = new Scanner(System.in);
		Integer cpf;
		String linha = "";

		System.out.println("Entre com o cpf do usuario a ser pesquizado:\n");
		cpf = read.nextInt();

		try {
			FileReader fileReader = new FileReader(ARQUIVO_USUARIO);
			BufferedReader lerArq = new BufferedReader(fileReader);
			linha = lerArq.readLine();

			while (linha != null) {
				if (linha.substring(20, 40).trim().equals(cpf.toString())) {
					System.out.println(linha);
					return criaObjeto(linha);
				}

				linha = lerArq.readLine();
			}
			lerArq.close();
			fileReader.close();
		} catch (Exception e) {
}

		return criaObjeto(linha);

	}
	
//---------------------------------------SOBRECARGA DE M�TODO-------------------------------------------------	
	@SuppressWarnings("resource")
	public  Boolean pesquisarUsuario(Integer senha, String matricula) {
		Usuario usuario = new Usuario(null, null);
		
		String linha = "";		

		try {
			FileReader fileReader = new FileReader(ARQUIVO_USUARIO);
			BufferedReader lerArq = new BufferedReader(fileReader);
			linha = lerArq.readLine();

			while (linha != null) {
				if (linha.substring(120, 140).trim().equals(senha.toString()) && linha.substring(140, 160).trim().equals(matricula)) {
					System.out.println(linha);
					return true;
				}

				linha = lerArq.readLine();
			}
			lerArq.close();
			fileReader.close();
		} catch (Exception e) {
}

		return false;

	}

	public void listarUsuarios() {

		try {
			// Indicamos o arquivo que ser� lido
			FileReader fileReader = new FileReader(ARQUIVO_USUARIO);

			// Criamos o objeto bufferReader que nos oferece o m�todo de leitura
			// readLine()

			BufferedReader bufferedReader = new BufferedReader(fileReader);

			// String que ir� receber cada linha do arquivo
			String linha = "";

			while ((linha = bufferedReader.readLine()) != null) {
				// Aqui imprimimos a linha
				System.out.println(linha);
			}

			// liberamos o fluxo dos objetos
			// ou fechamos o arquivo
			fileReader.close();
			bufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("resource")
	public boolean deletarUsuarios() {
		Scanner read = new Scanner(System.in);
		Integer cpf;
		String linha = "";

		System.out.println("Entre com o cpf do usuario a ser exclu�do:\n");
		cpf = read.nextInt();
		boolean deletou = false;

		// File TEMPORARIO = new
		// File("C:\\Users\\Luiz\\Desktop\\trabalho\\temporario.txt");
		try {
			TEMPORARIO_USUARIO.createNewFile();
		} catch (IOException e1) {
			System.out.println("N�o foi poss�vel criar o arquivo!");
			e1.printStackTrace();
		}

		try {

			BufferedReader readFile = new BufferedReader(new FileReader(ARQUIVO_USUARIO));
			PrintWriter writer = new PrintWriter(new FileWriter(TEMPORARIO_USUARIO));
			
			linha = readFile.readLine();
			while ((linha != null)) {
				if (linha.substring(20, 40).trim().equals(cpf.toString())) {
					System.out.println("O usuario foi removido com sucesso da base de dados!");
					deletou = true;

				} else {
					writer.println(linha);
				}
				linha = readFile.readLine();
			}
			if (deletou != true && (linha == null)) {
				System.out.println("O usuario a ser exclu�do n�o existe na base de dados!");
				writer.close();
				readFile.close();
				TEMPORARIO_USUARIO.delete();
				return false;
			} else
				writer.close();
				readFile.close();				
				ARQUIVO_USUARIO.delete();
				TEMPORARIO_USUARIO.renameTo(ARQUIVO_USUARIO);
			return true;

		} catch (Exception e) {
			System.out.println("ERRO: N�o foi poss�vel deletar a informa��o!");
		}
		return true;
	}

	@SuppressWarnings("resource")
	public boolean editarUsuario() {

		Usuario usuario = new Usuario(nome, cpf, rg, telefone, email, endereco, senha, matricula);
		Scanner read = new Scanner(System.in);
		Integer cpf;
		String linha = "";

		System.out.println("Entre com o cpf do usuario a ser editado:\n");
		cpf = read.nextInt();
		boolean editou = false;

		// File TEMPORARIO = new
		// File("C:\\Users\\Luiz\\Desktop\\trabalho\\temporario.txt");
		try {
			TEMPORARIO_USUARIO.createNewFile();
		} catch (IOException e1) {
			System.out.println("N�o foi poss�vel criar o arquivo!");
			e1.printStackTrace();
		}

		try {

			BufferedReader readFile = new BufferedReader(new FileReader(ARQUIVO_USUARIO));
			PrintWriter writer = new PrintWriter(new FileWriter(TEMPORARIO_USUARIO));
			linha = readFile.readLine();

			while (linha != null) {
				if (linha.substring(20, 40).trim().equals(cpf.toString())) {

					System.out.println("Nome da usu�rio: ");
					String nome = read.next();
					usuario.setNome(nome);					
					
					usuario.setCpf(cpf);

					System.out.println("RG: ");
					String rg = read.next();
					usuario.setRg(rg);		

					System.out.println("Telefone: ");
					String telefone = read.next();
					usuario.setTelefone(telefone);

					System.out.println("Email: ");
					String email = read.next();
					usuario.setEmail(email);
					
					System.out.println("Endere�o: ");
					String endereco = read.next();
					usuario.setEmail(endereco);
					
					System.out.println("Senha: ");
					Integer senha = read.nextInt();
					usuario.setSenha(senha);
					
					System.out.println("Matricula: ");
					String matricula = read.next();
					usuario.setMatricula(matricula);
					// gravaArquivoTemporario(aviao);

					System.out.println("O elemento foi editado com sucesso!");
					editou = true;

				} else {
					writer.println(linha);
				}
				linha = readFile.readLine();
			}
			if (editou != true && (linha == null)) {
				System.out.println("O elemento a ser editado n�o existe!");
				writer.close();
				readFile.close();
				TEMPORARIO_USUARIO.delete();
				return false;
			} else
				writer.close();
			readFile.close();
			gravaArquivoTemporarioClinte(usuario);
			ARQUIVO_USUARIO.delete();
			TEMPORARIO_USUARIO.renameTo(ARQUIVO_USUARIO);
			return true;

		} catch (Exception e) {
			System.out.println("ERRO: N�o foi poss�vel editar a informa��o!");
		}
		return true;

	}

}

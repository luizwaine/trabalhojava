package br.com.companhiaAerea.jpanel;

import java.awt.EventQueue;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

public class Janela1 extends JInternalFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Janela1 frame = new Janela1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Janela1() {
		setTitle("Cadastro de Avi\u00F5es");
		
		JPanel panelBotoes = new JPanel();
		
		JPanel panelRegister = new JPanel();
		
		JPanel panelRodape = new JPanel();
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(panelBotoes, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(panelRodape, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(panelRegister, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 433, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(panelBotoes, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panelRegister, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(panelRodape, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		
		JButton btnVoltar = new JButton("Voltar");
		
		JButton Salvar = new JButton("Salvar");
		
		JButton btnNewButton_1 = new JButton("deletar");
		
		JButton btnNewButton_3 = new JButton("Editar");
		GroupLayout gl_panelRodape = new GroupLayout(panelRodape);
		gl_panelRodape.setHorizontalGroup(
			gl_panelRodape.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panelRodape.createSequentialGroup()
					.addContainerGap(116, Short.MAX_VALUE)
					.addComponent(btnNewButton_3, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnNewButton_1)
					.addGap(18)
					.addComponent(btnVoltar)
					.addGap(18)
					.addComponent(Salvar, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
					.addGap(1))
		);
		gl_panelRodape.setVerticalGroup(
			gl_panelRodape.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panelRodape.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(gl_panelRodape.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnVoltar)
						.addComponent(Salvar)
						.addComponent(btnNewButton_1)
						.addComponent(btnNewButton_3)))
		);
		panelRodape.setLayout(gl_panelRodape);
		
		JLabel lblNumeroLugares = new JLabel("Numero lugares");
		
		JLabel lblFabricante = new JLabel("Fabricante");
		
		JLabel lblModelo = new JLabel("Modelo");
		
		JLabel lblRegistro = new JLabel("Registro");
		
		textField = new JTextField();
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		GroupLayout gl_panelRegister = new GroupLayout(panelRegister);
		gl_panelRegister.setHorizontalGroup(
			gl_panelRegister.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelRegister.createSequentialGroup()
					.addGap(10)
					.addGroup(gl_panelRegister.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelRegister.createSequentialGroup()
							.addComponent(lblRegistro)
							.addGap(38)
							.addComponent(textField, GroupLayout.PREFERRED_SIZE, 167, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panelRegister.createSequentialGroup()
							.addComponent(lblNumeroLugares)
							.addGap(4)
							.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, 166, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panelRegister.createSequentialGroup()
							.addComponent(lblFabricante)
							.addGap(28)
							.addComponent(textField_2, GroupLayout.PREFERRED_SIZE, 166, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panelRegister.createSequentialGroup()
							.addComponent(lblModelo)
							.addGap(45)
							.addComponent(textField_3, GroupLayout.PREFERRED_SIZE, 166, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(178, Short.MAX_VALUE))
		);
		gl_panelRegister.setVerticalGroup(
			gl_panelRegister.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelRegister.createSequentialGroup()
					.addGap(11)
					.addGroup(gl_panelRegister.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelRegister.createSequentialGroup()
							.addGap(3)
							.addComponent(lblRegistro))
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(6)
					.addGroup(gl_panelRegister.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelRegister.createSequentialGroup()
							.addGap(3)
							.addComponent(lblNumeroLugares))
						.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(6)
					.addGroup(gl_panelRegister.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelRegister.createSequentialGroup()
							.addGap(3)
							.addComponent(lblFabricante))
						.addComponent(textField_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(6)
					.addGroup(gl_panelRegister.createParallelGroup(Alignment.LEADING)
						.addComponent(lblModelo)
						.addComponent(textField_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(72, Short.MAX_VALUE))
		);
		panelRegister.setLayout(gl_panelRegister);
		GroupLayout gl_panelBotoes = new GroupLayout(panelBotoes);
		gl_panelBotoes.setHorizontalGroup(
			gl_panelBotoes.createParallelGroup(Alignment.LEADING)
				.addGap(0, 443, Short.MAX_VALUE)
		);
		gl_panelBotoes.setVerticalGroup(
			gl_panelBotoes.createParallelGroup(Alignment.LEADING)
				.addGap(0, 33, Short.MAX_VALUE)
		);
		panelBotoes.setLayout(gl_panelBotoes);
		getContentPane().setLayout(groupLayout);
		setBounds(100, 100, 450, 291);		
	
	
	}
}
